# Compte rendu

## Ce que j'ai appris
J'ai découvert en une semaine beaucoup de choses sur React
J'avais avant une vague connaissance de React et avec cette semaine j'ai pu découvrir le système de useContext, useReducer et renforcer mes connaissances sur les Hooks useEffect,, useState, setState, etc.

## Ce que j'ai appris pendant le projet
On a utilisé tout ce qu'on a vu pendant le cours, ce qui fût très instructif
Avec la technologie imposées, j'ai découvert Chakra UI, une librairie qui me permet de faire du css plus facilement.
Il permet également de faire des composants CSS, qui est la première fois que je l'utilise