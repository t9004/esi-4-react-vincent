import React, { useEffect, useState } from 'react';
import Movie from './components/Movie';

function App() {
    const [movies, setMovies] = useState([]);

    const updMovies = () => {
        setMovies([]);
        fetch('https://swapi.dev/api/films/')
            .then(response => response.json())
            .then((response) => {
                setMovies(response.results)
            })
    }

    useEffect(() => {
        console.log('update movies')
    }, [movies])

    return (
        <div>
            <button onClick={updMovies}>Recharger</button>
            <div className="movieContainer">
                { movies.map(m => <Movie movie={m}/>) }
            </div>
        </div>
    );
}

export default App;
