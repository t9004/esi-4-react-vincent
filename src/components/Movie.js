import React from 'react';

const Movie = (props) => {
    const {id, title, opening_crawl, director, producer} = props.movie;
    return (
        <div className="movie">
            <h1>{title}</h1>
            <div>
                <img src='../images/movie.png'/>
            </div>
            <div>{opening_crawl}</div>
            <div>{director}</div>
            <div>{producer}</div>
        </div>
    )
}

export default Movie;
